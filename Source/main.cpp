//
//  main.cpp
//  CommandLineTool
//  Software Development for Audio
//

#include <iostream>
#include "Array.h"

int max (int a, int b);
float fMax (float fa, float fb);
bool testFunctions();
double dMax(double da, double db);

int main (int argc, const char* argv[])
{
    testFunctions();
    testArray();
}

int max(int a, int b)// Max Function
{
    if (a < b)
        
        return b;
    else
        return a;
    
}

float fMax(float fa, float fb)
{
    if (fa < fb)
        
        return fb;
    else
        return fa;
    
}

double dMax(double da, double db)
{
    if (da < db)
        
        return db;
    else
        return da;
}

bool testFunctions()
{
//int Max
    if (max (5, 10) != 10)
    {
        std::cout << "error with max\n";
        return false;
    }
//float fMax
    if (fMax (10.8, 5.2) != 10.8f)
    {
        std::cout << "error with fMax\n";
        return false;
    }
//double dMax
    if (dMax (5.053, 10.089) != 10.089)
    {
        std::cout << "error with dMax\n";
        return false;
    }
    
    
    std::cout << "all functions tests passed\n";
    return true;
    

    
   
}



