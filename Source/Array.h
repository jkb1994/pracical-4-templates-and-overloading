//
//  Array.h
//  CommandLineTool
//
//  Created by Jacobs Laptop  on 09/10/2014.
//  Copyright (c) 2014 Tom Mitchell. All rights reserved.
//

#ifndef H_ARRAY
#define H_ARRAY

#include <iostream>

class Array
{
public:
    
    Array()
    {
      arraysize = 0;
      fp = nullptr;
    }
    ~Array()
    {
      
    }
    void add (float itemValue)
    {
        // Add new value
        float *fpNew = new float[arraysize + 1];
        
        // Add new data
        for (int i; i < arraysize; i++)
        {
            fpNew[i] = fp[i];
        }
        
        // Add new value
        fpNew[arraysize] = itemValue;
        
        // delete old array
        if (fp != nullptr)
        {
           delete [] fpNew;
        }
        
        // make data point to new array
        fp = fpNew;
        
        arraysize++;
        
        
    }
    float get (int index)
    {
        
        return fp[index];
    }
    int size()
    {
        
        return arraysize;
    }
private:
    
    float *fp = nullptr;
    int arraysize;
    
};

bool testArray()
{
    Array array;
    const float testArray[] = {0.f, 1.f, 2.f, 3.f, 4.f, 5.f};
    const int testArraySize = sizeof (testArray) / sizeof (testArray[0]);
    
    if (array.size() != 0)
    {
        std::cout << "size is incorrect\n";
        return false;
    }
    
    for (int i = 0; i < testArraySize; i++)
    {
        array.add (testArray[i]);
        
        if (array.size() != i + 1)
        {
            std::cout << "size is incorrect\n";
            return false;
        }
        
        if (array.get (i) != testArray[i])
        {
            std::cout << "value at index "<< i << " recalled incorrectly\n" ;
            return false;
        }
    }
    
    //    //removing first
    //    array.remove (0);
    //    if (array.size() != testArraySize - 1)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    for (int i = 0; i < array.size(); i++)
    //    {
    //        if (array.get(i) != testArray[i+1])
    //        {
    //            std::cout << "problems removing items\n";
    //            return false;
    //        }
    //    }
    //
    //    //removing last
    //    array.remove (array.size() - 1);
    //    if (array.size() != testArraySize - 2)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    for (int i = 0; i < array.size(); i++)
    //    {
    //        if (array.get(i) != testArray[i + 1])
    //        {
    //            std::cout << "problems removing items\n";
    //            return false;
    //        }
    //    }
    //
    //    //remove second item
    //    array.remove (1);
    //    if (array.size() != testArraySize - 3)
    //    {
    //        std::cout << "with size after removing item\n";
    //        return false;
    //    }
    //
    //    if (array.get (0) != testArray[1])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    //
    //    if (array.get (1) != testArray[3])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    //
    //    if (array.get (2) != testArray[4])
    //    {
    //        std::cout << "problems removing items\n";
    //        return false;
    //    }
    std::cout << "All array tests passed\n" ;
    return true;
}




#endif /* H_ARRAY */
